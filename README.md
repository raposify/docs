```This document is under construction```

# Raposify Transcribed Functions Manual (RTFM)

> Algorithms and heuristics for humans

This repository is a collection of successes (big or small) that resulted from research, 
troubleshooting, cursing, or outsmarting. 
The content you find here is either a **resource** (like a template you use repeatedly),
a **procedure** (step-by-step instructions), 
or a **guideline** (e.g. Remote work etiquette).
Check out the folders in the repository to get a feel for what's out there.

**Do your part!** If there's a chance that a task could be repeated, add a page. 
If you've found your way around an outdated procedure, do the next person a favour 
and update the content.

Don't have time right now? [Create an issue](https://gitlab.com/raposify/docs/issues)!
(Plus, by logging an issue, you can assign it to someone else...:smirk:).

#### Remember to use
* ```As few words as possible```
* ```Numbered or bullet points whenever possible```
* ```Pictures```

## Table of Contents
1. [Formatting](#formatting)
 1. [Process](#process)
 1. [Procedure](#procedure)
1. [Coding](#coding)
1. [Commits](#commits)

## Formatting
Any write-up (like a process, procedure, app readme or guideline) lives in
a repository and so is written in [Markdown](https://docs.gitlab.com/ee/user/markdown.html#code-and-syntax-highlighting).
The [StackEdit Editor](https://stackedit.io/editor) is helpful for those who don't think
in Markdown.
In order to have your Markdown displayed with proper formatting, **save your file 
with the extension .md**. If you want your document to displayed upon entering a
repository folder, save the file as README.md.

Each document has the following sections in the given order. Here it is with Markdown: 

```
```This document is under construction``` (Take out this line when page is ready for consumption)

# Page title

> Type of document/app (e.g. "Business process" or "A Meteor application")

Introduction/overview paragraph

> [Report a bug or request a new feature](https://gitlab.com/raposify/<repository>/issues/new) (Keep this if you're writing for an app)
> [Report an error](https://gitlab.com/raposify/<repository>/issues/new) (Use this for everything else)

## Table of Contents
1. [Section 1](#section-1)
 1. [Subsection A](#subsection-a]
 *  [Subpoint B](#subpoint-b]
1. [Section 2](#section-2)
1. [Section 3](#section-3)
1. [References](#references)

## Section 1
Stuff about Section 1.

### Subsection A
Stuff about Subsection A.

### Subpoint B
Stuff about Subpoint B.

## Section 2
Stuff about Section 2.

## Section 3
Stuff about Section 3.

## References
Only have this section if needed. Reference sites like this:
* [Awesome website](https://google.com)
* [Another awesome website](https://raposify.com)

> [Report a bug or request a new feature](https://gitlab.com/raposify/<repository>/issues/new) (Keep this if you're writing for an app)
> [Report an error](https://gitlab.com/raposify/<repository>/issues/new) (Use this for everything else)
```

### Process
Processes are depicted as flowcharts ( - please [follow convention](https://www.smartdraw.com/flowchart/flowchart-symbols.htm)). 
Steps in the flowchart link to [procedures](#procedure) or other processes, if applicable.

When you add a new process, ask yourself if it can be linked to an existing process or procedure.

* shape convention
* colour scheme
* links
 * URLs
 * Other processes
 * Procedures

### Procedure

When you add a new procedure, ask yourself if it can be linked to an existing process or procedure.
* Succinct
* Bullets & sub-bullets
* Numbered (with 1s) vs. bullets
* Break up procedure into sections: then TOC

## Coding
For any Raposify project, Canadian spelling is preferred. However, if we are forking or extending an existing repository,
then precedence is given to the language of that original repository.

## Commits
Anytime you commit a change to the repository, there should be an issue that you can trace back to.
For example, this page you're reading was done for Issue #2 in this repository: "Documentation style guide".
So when I commit my changes to this page, my commit message is in this format:

```
#2: new section on commits
```

If you don't see an issue for the change you need to make, please make an issue 
first so it can be tracked on the business side of things.